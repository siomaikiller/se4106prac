<?php

namespace CpuSeDept\PracBundle\Controller;

use CpuSeDept\PracBundle\Entity\TownRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/town")
 */
class TownController extends Controller
{
    /**
     * @Route( "/evacuate/{id}" )
     * @Template
     */
    public function evacuateAction($id) 
    {
        $repo = $this->getDoctrine()->getRepository("CpuSeDeptPracBundle:Town");
        /* @var $repo TownRepository */
        return ['evacuees' => $repo->findWomenAndChildrenFromTown($id) ];
    }
    
    /**
     * @Route( "/list-seniors/{id}" )
     * @Template
     */
    public function listSeniorsAction($id) 
    {
        $repo = $this->getDoctrine()->getRepository("CpuSeDeptPracBundle:Town");
        /* @var $repo TownRepository */
        return ['seniors' => $repo->findElderlyFromTown($id) ];
    }
    
    /**
     * @Route( "/find-town" )
     * @Route( "/" )
     * @Template
     */
    public function findTownAction(Request $request) 
    {
        $repo = $this->getDoctrine()->getRepository("CpuSeDeptPracBundle:Town");
        /* @var $repo TownRepository */
        $q = $request->query->get('q') ?: '';
        return ['matches' => $repo->findTownLikeName($q) ];
    }
    
     /**
     * @Route( "/view/{id}" )
     * @Template
     */
    public function viewAction($id) 
    {
        $repo = $this->getDoctrine()->getRepository("CpuSeDeptPracBundle:Town");
        /* @var $repo TownRepository */
        return ['town' => $repo->find($id) ];
    }
    
     /**
     * @Route( "/list-pwds/{id}" )
     * @Template
     */
    public function listPwdsAction($id) 
    {
        $repo = $this->getDoctrine()->getRepository("CpuSeDeptPracBundle:Citizen");
        /* @var $repo TownRepository */
        return ['citizens' => $repo->findByHometown($id) ];
    }
}
